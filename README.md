[![Codacy Badge](https://api.codacy.com/project/badge/Grade/cd5f14dcfd0943a3b20a60280e703eea)](https://www.codacy.com/app/bitandblack/highlighter-styles?utm_source=wirbelwild@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/highlighter-styles&amp;utm_campaign=Badge_Grade)

# Highlighter Styles

This package provides some syntax highlighting styles and is meant to use along with the [Syntax Highlighter](https://bitbucket.org/wirbelwild/highlighter) by [Bit&Black](https://www.bitandblack.com).

## Installation

This package is available for the use with [NPM](https://www.npmjs.com/package/bitandblack-highlighter) and [Yarn](https://yarnpkg.com/en/package/bitandblack-highlighter). Use one of them to install: 

### Node

`$ npm install bitandblack-highlighter-styles` 

### Yarn 

`$ yarn add bitandblack-highlighter-styles` 

## Usage

Take one of the `.css` or `.scss` files and add them to your projects stylesheet. 

## Help 

If you need help feel free to contact us under `highlighter@bitandblack.com`.
